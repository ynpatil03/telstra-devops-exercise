package com.telstra.ecom.product.service;

import java.util.List;

import com.telstra.ecom.product.model.Product;
/*
 * This interface provides all methods to access the functionality. See ArticleServiceImpl for implementation.
 * 
 * @author crossover
 */
public interface ProductService {
	
	/**
	 * 
	 * get all products
	 */
	public Iterable<Product> findAll();
	/*
	 * Save the default Product.
	 */
	Product save(Product article);
	
	/*
	 * FindById will find the specific Product form list.
	 * 
	 */
	Product findById(Long id);
	
	/*
	 * Delete a particular Product with id
	 */
	void delete(Long id);
	
	Product findByProductid(String productId);
	
	
	
}
