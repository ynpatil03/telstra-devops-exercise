package com.telstra.ecom.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.ecom.product.model.Product;
import com.telstra.ecom.product.service.ProductService;


@RestController
public final class ProductController {
	
	@Autowired
	ProductService productService;
	
	@GetMapping(path="products")
	public ResponseEntity<Iterable<Product>> getProducts()
	{
		Iterable<Product> product = productService.findAll();
		if (product!=null) {
			return new  ResponseEntity<Iterable<Product>>(product,HttpStatus.OK);
		}
		return new ResponseEntity<Iterable<Product>>(HttpStatus.NOT_FOUND);
	}
	

	@PostMapping(path="products")
	public ResponseEntity<Product> createProduct(@RequestBody  Product product)
	{
		return new ResponseEntity<Product>(productService.save(product),HttpStatus.CREATED);	
	}
	
	@GetMapping(path="products/{product-id}")
	public ResponseEntity<Product> getProductByProductId(@PathVariable("product-id") String productId)
	{
		Product product = productService.findByProductid(productId);
		if (product!=null)
			return new  ResponseEntity<Product>(product,HttpStatus.OK);
		return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
	}
	
	@PutMapping(path="products/{product-id}")
	public ResponseEntity<Product> updateProduct(@PathVariable("product-id") Long id,@RequestBody Product product)
	{	
		return new ResponseEntity<Product>(productService.save(product),HttpStatus.OK);
	}
	@DeleteMapping(path="products/{product-id}")
	public ResponseEntity<Product> deleteProductById(@PathVariable("product-id") Long id)
	{
		productService.delete(id);
		return new ResponseEntity<Product>(HttpStatus.OK);
	}


}
