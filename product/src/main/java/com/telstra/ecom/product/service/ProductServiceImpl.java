package com.telstra.ecom.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telstra.ecom.product.model.Product;
import com.telstra.ecom.product.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductRepository ProductRepository;
	
	public Iterable<Product> findAll()
	{
		return ProductRepository.findAll();
	}
	public Product save (Product Product)
	{
		return ProductRepository.save(Product);
	}
	public Product findById(Long id)
	{
		return ProductRepository.findById(id).orElse(null);
	}
	public void delete(Long id) {
		ProductRepository.deleteById(id);
	}

	public Product findByProductid(String productId) {
		return ProductRepository.findByProductId(productId);
	}
	
	
	
}