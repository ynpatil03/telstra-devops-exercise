package com.telstra.ecom.product.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.telstra.ecom.product.model.Product;


@RepositoryRestResource(exported=false)
public interface ProductRepository extends PagingAndSortingRepository<Product,Long> {


	Product findByProductId(String productId);
}
