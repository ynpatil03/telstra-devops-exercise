package com.telstra.ecom.product.controller;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.telstra.ecom.product.model.Product;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

	
	@Autowired
	private TestRestTemplate template;
	
	@Before
	public void setup() throws Exception {
		
	}
	
	@Test
	public void testProductShouldBeCreated() throws Exception {
		HttpEntity<Object> product=  getHttpEntity("{\"productId\": \"12445dsd234\", \"category\": \"Modile\",\"productName\": \"Samsung\" ,\"productModel\": \"GalaxyNote\", \"price\": \"700\", \"availableQuantity\": \"10\"}");
		ResponseEntity<Product>  resultAsset= template.postForEntity("/products",product , Product.class);
		Assert.assertNotNull(resultAsset.getBody().getId());
	}
	
	
	@Test
	public void testGetAllProduct() throws Exception {
		HttpEntity<Object> product=  getHttpEntity("{\"productId\": \"12445dsd234\", \"category\": \"Modile\",\"productName\": \"Samsung\" ,\"productModel\": \"GalaxyNote\", \"price\": \"700\", \"availableQuantity\": \"10\"}");
		ResponseEntity<Product>  resultAsset3= template.postForEntity("/products",product , Product.class);
		ResponseEntity<List>  resultAsset= template.getForEntity("/products/all", List.class);
		Assert.assertNotNull(resultAsset.getBody());
	}
	private  HttpEntity<Object> getHttpEntity(Object body)
	{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return  new HttpEntity<Object>(body,headers);	
	}
}
