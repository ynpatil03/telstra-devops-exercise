package com.telstra.ecom.cart.service;

import java.util.List;

import com.telstra.ecom.cart.model.Cart;
import com.telstra.ecom.cart.model.Product;

public interface CartService {
	

	/*
	 * Returns all the Comments related to article along with Pagination information.
	 */
	List<Cart> findAll(Long productId);
	
	/*
	 * Save the default article.
	 */
	 Cart save(Cart cart);
	 
	 List<Cart> findByuuId(String uuid);
	 List<Product> getAllProducts();
	 public Product getProductByProductId(String productId);


}
