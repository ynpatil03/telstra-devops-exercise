package com.telstra.ecom.cart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.ecom.cart.model.Cart;
import com.telstra.ecom.cart.model.Product;
import com.telstra.ecom.cart.service.CartService;

@RestController
public class CartController {
	@Autowired
	CartService cartService;
	
	@PostMapping(path="rest/v1/users/{uuid}/cart")
	public ResponseEntity<Cart> createCart(@PathVariable(value="uuid")String uuid, @RequestBody Cart cart)
	{
		cart.setUuid(uuid);
		Product product =cartService.getProductByProductId(cart.getProductId());
		//To do - set value from product and calculate total price
		return new ResponseEntity<Cart>(cartService.save(cart),HttpStatus.CREATED);
	}
	
	@GetMapping(path="rest/v1/products")
	public ResponseEntity<List<Product>> getProducts()
	{
		return new ResponseEntity<List<Product>>(cartService.getAllProducts(),HttpStatus.OK);	
	}
	@GetMapping(path="rest/v1/users/{uuid}/cart")
	public ResponseEntity<List<Cart>> getCarts(@PathVariable("uuid") String uuid)
	{
		return new ResponseEntity<List<Cart>>(cartService.findByuuId(uuid),HttpStatus.OK);	
	}
}
