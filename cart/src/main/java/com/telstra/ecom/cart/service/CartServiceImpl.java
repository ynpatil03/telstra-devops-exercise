package com.telstra.ecom.cart.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.ecom.cart.model.Cart;
import com.telstra.ecom.cart.model.Product;
import com.telstra.ecom.cart.repository.CartRepository;

@Service
public class CartServiceImpl implements CartService{

	@Autowired
	CartRepository cartRepository;
	
	
	/*
	 * Returns all the Carts related to article along with Pagination information.
	 */
	public List<Cart> findAll(Long articleId){
		return cartRepository.findAll();
	}
	
	
	public Product getProductByProductId(String productId) {
		final String uri = "http://localhost:8088/products/"+productId;
	     
	    RestTemplate restTemplate = new RestTemplate();
	    Product result = restTemplate.getForObject(uri, Product.class);
	     
	    System.out.println(result);
		return result;
	}
	public Cart save(Cart cart)
	{
		return cartRepository.save(cart);
	}

	public List<Product> getAllProducts() {
		final String uri = "http://localhost:8088/products";
	     
	    RestTemplate restTemplate = new RestTemplate();
	    List<Product> result = (List<Product>)restTemplate.getForObject(uri, List.class);
	     
	    System.out.println(result);
		return result;
	}
	public List<Cart> findByuuId(String uuid){
		
		return cartRepository.findByUuid(uuid);
	}

}
