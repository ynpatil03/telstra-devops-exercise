package com.telstra.ecom.cart.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.telstra.ecom.cart.model.Cart;

@RepositoryRestResource(exported=false)
public interface CartRepository extends PagingAndSortingRepository<Cart,Long> {
	
	@Override
	List<Cart> findAll();
	
	List<Cart> findByUuid(String uuid);
}
